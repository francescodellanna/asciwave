A simple script written in sed to print an asci wave

By running the example you should get the following output:

./example.sh

   _   _   _   _   _   _  
__| |_| |_| |_| |_| |_| |_ clk
   ___     ___     ___    
__|   |___|   |___|   |___ clk_div2
           _______________
__________|                data

For more info visit my blog at https://fdblog.xyz/circuits/asciwave/